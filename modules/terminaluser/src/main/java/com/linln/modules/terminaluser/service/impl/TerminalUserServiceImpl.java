package com.linln.modules.terminaluser.service.impl;

import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import com.linln.modules.system.domain.Dept;
import com.linln.modules.system.domain.User;
import com.linln.modules.system.service.DeptService;
import com.linln.modules.terminaluser.domain.TerminalUser;
import com.linln.modules.terminaluser.repository.TerminalUserRepository;
import com.linln.modules.terminaluser.service.TerminalUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author likm
 * @date 2021/07/14
 */
@Service
public class TerminalUserServiceImpl implements TerminalUserService {

    @Autowired
    private TerminalUserRepository terminalUserRepository;

    @Autowired
    private DeptService deptService;

    /**
     * 根据ID查询数据
     *
     * @param id 主键ID
     */
    @Override
    public TerminalUser getById(Long id) {
        return terminalUserRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     *
     * @param terminalUser 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<TerminalUser> getPageList(TerminalUser terminalUser) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return terminalUserRepository.findAll(new Specification<TerminalUser>() {

            @Override
            public Predicate toPredicate(Root<TerminalUser> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> preList = new ArrayList<>();
                if (terminalUser.getId() != null) {
                    preList.add(cb.equal(root.get("id").as(Long.class), terminalUser.getId()));
                }
                if (terminalUser.getPhone() != null) {
                    preList.add(cb.equal(root.get("phone").as(String.class), terminalUser.getPhone()));
                }
                if (terminalUser.getName() != null) {
                    preList.add(cb.like(root.get("name").as(String.class), "%" + terminalUser.getName() + "%"));
                }
                if (terminalUser.getTerminalId() != null) {
                    preList.add(cb.like(root.get("terminalId").as(String.class), "%" + terminalUser.getTerminalId() + "%"));
                }
                if (terminalUser.getTerminalKey() != null) {
                    preList.add(cb.like(root.get("terminalKey").as(String.class), "%" + terminalUser.getTerminalKey() + "%"));
                }
                if (terminalUser.getCheckStatus() != null) {
                    preList.add(cb.equal(root.get("checkStatus").as(Integer.class), terminalUser.getCheckStatus()));
                }
                if (terminalUser.getDept() != null) {
                    // 联级查询部门
                    Dept dept = terminalUser.getDept();
                    List<Long> deptIn = new ArrayList<>();
                    deptIn.add(dept.getId());
                    List<Dept> deptList = deptService.getListByPidLikeOk(dept.getId());
                    deptList.forEach(item -> deptIn.add(item.getId()));

                    Join<User, Dept> join = root.join("dept", JoinType.INNER);
                    CriteriaBuilder.In<Long> in = cb.in(join.get("id").as(Long.class));
                    deptIn.forEach(in::value);
                    preList.add(in);
                }

                // 数据状态
                if (terminalUser.getStatus() != null) {
                    preList.add(cb.equal(root.get("status").as(Byte.class), terminalUser.getStatus()));
                }

                Predicate[] pres = new Predicate[preList.size()];
                return query.where(preList.toArray(pres)).getRestriction();
            }

        }, page);
    }

    /**
     * 保存数据
     *
     * @param terminalUser 实体对象
     */
    @Override
    public TerminalUser save(TerminalUser terminalUser) {
        return terminalUserRepository.save(terminalUser);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return terminalUserRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}