package com.linln.modules.terminaluser.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author 小懒虫 <auntvt＠163.com>
 * @date 2021/3/19
 */
@ComponentScan(basePackages = "com.linln.modules.terminaluser")
@EnableJpaRepositories(basePackages = "com.linln.modules.terminaluser")
@EntityScan(basePackages = "com.linln.modules.terminaluser")
public class TerminalUserAutoConfig {
}
