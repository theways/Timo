package com.linln.modules.terminaluser.service;

import com.linln.common.enums.StatusEnum;
import com.linln.modules.terminaluser.domain.TerminalUser;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author likm
 * @date 2021/07/14
 */
public interface TerminalUserService {

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    Page<TerminalUser> getPageList(TerminalUser terminalUser);

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    TerminalUser getById(Long id);

    /**
     * 保存数据
     * @param terminalUser 实体对象
     */
    TerminalUser save(TerminalUser terminalUser);

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional(rollbackFor = Exception.class)
    Boolean updateStatus(StatusEnum statusEnum, List<Long> idList);
}