package com.linln.modules.terminaluser.repository;

import com.linln.modules.system.repository.BaseRepository;
import com.linln.modules.terminaluser.domain.TerminalUser;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author likm
 * @date 2021/07/14
 */
public interface TerminalUserRepository extends BaseRepository<TerminalUser, Long>, JpaSpecificationExecutor<TerminalUser> {
}