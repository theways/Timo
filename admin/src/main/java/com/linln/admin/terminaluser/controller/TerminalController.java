package com.linln.admin.terminaluser.controller;

import com.linln.admin.terminaluser.validator.TerminalUserValid;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.vo.ResultVo;
import com.linln.component.shiro.ShiroUtil;
import com.linln.modules.system.domain.User;
import com.linln.modules.terminaluser.domain.TerminalUser;
import com.linln.modules.terminaluser.service.TerminalUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * @Author likm
 * @Date 2021/7/15
 * @Description //TODO
 * @Version 1.0
 **/
@Controller
@RequestMapping("/terminal")
public class TerminalController {
    @Autowired
    private TerminalUserService terminalUserService;

    /**
     * 终端注册
     *
     * @param valid 验证对象
     */
    @PostMapping("/register")
    @ResponseBody
    public ResultVo register(@Validated TerminalUserValid valid, TerminalUser terminalUser) {
        // 复制保留无需修改的数据
        if (terminalUser.getId() != null) {
            TerminalUser beTerminalUser = terminalUserService.getById(terminalUser.getId());
            EntityBeanUtil.copyProperties(beTerminalUser, terminalUser);
        }
        //todo 生成key算法
        terminalUser.setTerminalKey(UUID.randomUUID().toString().substring(20));
        terminalUser.setCheckStatus(1);
        // 保存数据
        terminalUserService.save(terminalUser);
        return ResultVoUtil.success(Boolean.TRUE);
    }

    /**
     * 终端登录
     *
     * @param valid 验证对象
     */
    @PostMapping("/login")
    @ResponseBody
    public ResultVo login(@Validated TerminalUserValid valid, TerminalUser terminalUser) {
        // 复制保留无需修改的数据
        if (terminalUser.getId() != null) {
            TerminalUser beTerminalUser = terminalUserService.getById(terminalUser.getId());
            EntityBeanUtil.copyProperties(beTerminalUser, terminalUser);
        }
        //todo 生成key算法
        terminalUser.setTerminalKey(UUID.randomUUID().toString().substring(20));
        terminalUser.setCheckStatus(1);
        // 保存数据
        terminalUserService.save(terminalUser);
        return ResultVoUtil.success(Boolean.TRUE);
    }

}
