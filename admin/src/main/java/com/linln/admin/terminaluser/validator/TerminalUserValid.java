package com.linln.admin.terminaluser.validator;

import lombok.Data;

import java.io.Serializable;
import javax.validation.constraints.Pattern;

/**
 * @author likm
 * @date 2021/07/14
 */
@Data
public class TerminalUserValid implements Serializable {
    @Pattern(regexp = "^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$", message = "手机号码格式不正确")
    private String phone;
}