package com.linln.admin.terminaluser.controller;

import com.linln.admin.terminaluser.validator.TerminalUserValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import com.linln.component.shiro.ShiroUtil;
import com.linln.modules.system.domain.User;
import com.linln.modules.terminaluser.domain.TerminalUser;
import com.linln.modules.terminaluser.service.TerminalUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @author likm
 * @date 2021/07/14
 */
@Controller
@RequestMapping("/terminaluser")
public class TerminalUserController {

    @Autowired
    private TerminalUserService terminalUserService;

    private static final Long one = 1L;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("terminaluser:index")
    public String index(Model model, TerminalUser terminalUser) {
        // 判断是否拥有后台角色
        User user = ShiroUtil.getSubject();
        // 创建匹配器，进行动态查询匹配
        // 获取数据列表
        Page<TerminalUser> list = terminalUserService.getPageList(terminalUser);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        model.addAttribute("dept", user.getDept());
        return "/terminaluser/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("terminaluser:add")
    public String toAdd() {
        return "/terminaluser/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("terminaluser:edit")
    public String toEdit(@PathVariable("id") TerminalUser terminalUser, Model model) {
        model.addAttribute("terminalUser", terminalUser);
        return "/terminaluser/add";
    }

    /**
     * 保存添加/修改的数据
     *
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"terminaluser:add", "terminaluser:edit"})
    @ResponseBody
    public ResultVo save(@Validated TerminalUserValid valid, TerminalUser terminalUser) {
        User user = ShiroUtil.getSubject();
        // 复制保留无需修改的数据
        if (terminalUser.getId() != null) {
            TerminalUser beTerminalUser = terminalUserService.getById(terminalUser.getId());
            EntityBeanUtil.copyProperties(beTerminalUser, terminalUser);
        }
        //todo 生成key算法
        terminalUser.setTerminalKey(UUID.randomUUID().toString().substring(20));
        terminalUser.setCheckStatus(1);
        terminalUser.setDept(user.getDept());
        // 保存数据
        terminalUserService.save(terminalUser);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("terminaluser:detail")
    public String toDetail(@PathVariable("id") TerminalUser terminalUser, Model model) {
        model.addAttribute("terminalUser", terminalUser);
        return "/terminaluser/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("terminaluser:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (terminalUserService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}